#!/usr/bin/env ruby

require 'rubygems'
require 'find'

$: << '../lib'
require 'entry'
require 'load'

STRUCTURE_DESCR_OFFSET=7

# load as_blocks
$as_blocks={}
Find::find("../data/as-block") do |f|
  # skip non-files, temporary files and backup files
  next unless File::file?(f)
  next if (f.end_with?("~") or f.start_with?("~"))
  
  # load item
  item=DN42::load_file(f)
  
  # build range and add to map
  $as_blocks[item[:key]]=item
end

# now load aut-nums
$aut_nums={}
Find::find("../data/aut-num") do |f|
  # skip non-files, tempor  ary files and backup files
  next unless File::file?(f)
  next if (f.end_with?("~") or f.start_with?("~"))
  
  # load and add
  item=DN42::load_file(f)
  $aut_nums[item[:key]]=item
end

# now load all route objects
$routes={}
$routes.merge!(DN42::load_dir("../data/route"))
$routes.merge!(DN42::load_dir("../data/route6"))

# iterate over all routes
$routes.keys.each do |route|
  # get the route origin(can be multiple AS)
  origin=$routes[route][:entry].props[:origin]
  
  # skip routes without origin
  next unless origin
  origin=origin.map do |a|
    match=/AS(\d+)/.match(a)
    match.captures[0].to_i
  end
  
  # now add it to each aut-num object..
  origin.each do |o|
    if $aut_nums.include?(o)
      $aut_nums[o][:prefixes]=[] if $aut_nums[o][:prefixes].nil?
      $aut_nums[o][:prefixes] << route
    end
  end
end

# and now register children
$as_blocks.keys.each do |block|
  # iterate over all blocks and find children blocks
  $as_blocks.keys.each do |pchild|
    # must include the block..
    next unless block.include?(pchild.begin)
    next unless block.include?(pchild.end)
    next if pchild == block
    
    # add parents list unless it already exists
    $as_blocks[pchild][:parents]=[] unless $as_blocks[pchild].include?(:parents)
    
    # add to the list of parents
    $as_blocks[pchild][:parents] << block
    
    # sort parents by length
    $as_blocks[pchild][:parents].sort! {|a,b| (a.end - a.begin) <=> (b.end - a.begin)}
    
    # and most specific one gets to be my parent
    $as_blocks[pchild][:parent]=$as_blocks[pchild][:parents][0]
  end
end

# and now iterate over them *another* time to build the reverse
$as_blocks.keys.each do |block|
  # retrieve our value
  val=$as_blocks[block]
  
  # skip the root elements(ie. those without any parent)
  next unless val.include?(:parent)
  
  # now add childs element if it doesn't exist so far
  $as_blocks[val[:parent]][:childs]=[] unless $as_blocks[val[:parent]].include?(:childs)
  
  # and add ourself to that..
  $as_blocks[val[:parent]][:childs] << block
end


def print_structure(values, level)
  # don't do anything if there is nothing to be done
  return unless values.length > 0
  
  # now print each block
  values.each do |val|
    # load entry for that block
    entry=Entry.new($as_blocks[val][:file])
    
    # print correct level; AS block; descr
    print ("\t"*level)
    print "+ #{val.begin}-#{val.end}"
    print ("\t"*(STRUCTURE_DESCR_OFFSET-level))
    puts entry.props[:descr]
    
    # list all admin contacts
    if entry.props[:admin_c]
      entry.props[:admin_c].each do |adminc|
        print ("\t"*(level+1))
        puts "* admin-c: #{adminc}"
      end
    end
    
    # and output policy
    if entry.props.include?(:policy)
      print ("\t"*(level+1))
      puts "= policy: #{entry.props[:policy][0]}"
    end
    
    # calculate statistics
    ## number of possible ASN's
    c_possible=val.end - val.begin
    ## aut-nums in this range
    c_used_direct=$aut_nums.keys.reject{|asn| !val.include?(asn)}
    ## and by sub-allocation
    c_used_sub=0
    if $as_blocks[val].include?(:childs)
      # now remove any aut-nums that is in any child as-block
      $as_blocks[val][:childs].each do |child|
        c_used_direct=c_used_direct.reject{|asn| child.include?(asn)}
      end
      c_used_sub=$as_blocks[val][:childs].inject(0){|sum, block| sum + (block.end - block.begin)}
    end
    c_used_direct=c_used_direct.length
    ## total
    c_used=c_used_direct+c_used_sub
    ## and number of free ASN's
    c_free=c_possible-c_used
    
    # now output these statistics
    print ("\t"*(level+1))
    puts "% size=#{c_possible}; used_direct=#{c_used_direct}; used_sub-allocation=#{c_used_sub}; used_total=#{c_used}; free=#{c_free}"
    
    # now print the children if it has any..
    if $as_blocks[val][:childs]
      print_structure($as_blocks[val][:childs], level + 1)
    end
  end
end


# now print structure
puts "Structure:"
print_structure($as_blocks.keys.reject {|key| $as_blocks[key].include?(:parent)}, 1)
puts
puts "Per AS Summary:"
lines=[['ASN', 'AS-Name', 'v4', 'v6', 'tech-c']]
$aut_nums.keys.sort {|a,b| a <=> b}.each do |as|
  obj=$aut_nums[as]
  name=obj[:entry].props[:as_name][0]
  unless obj[:prefixes].nil?
    prefixes_v4=obj[:prefixes].select {|p| p.kind_of? NetAddr::CIDRv4}.length
    prefixes_v6=obj[:prefixes].select {|p| p.kind_of? NetAddr::CIDRv6}.length
  else
    prefixes_v4=0
    prefixes_v6=0
  end
  obj[:haz]={:ip4 => prefixes_v4, :ip6 => prefixes_v6}
  techc=obj[:entry].props[:tech_c]
  tech=techc ? techc[0] : ''
  
  lines << [as, name, prefixes_v4, prefixes_v6, tech]
end

def pretty_print(lines)
  max=[0]*(lines[0].length)
  0.upto(lines[0].length - 1) do |i|
    lines.each do |l|
      cur=l[i].to_s.length
      max[i]=cur if cur > max[i]
    end
  end
  lines.each do |l|
    print "\t"
    0.upto(lines[0].length - 1) do |i|
      print l[i].to_s
      print (" "*(max[i]-l[i].to_s.length))
      print "\t"
    end
    puts
  end
end
pretty_print(lines)

# summary..
# IPv6-only   #   %
# IPv4-only   #   %
# dualstack   #   %
# No IP       #   %
# Total       #
puts "\nSummary:"
lines=[['', '#', '%']]
ip6o=$aut_nums.keys.select {|a| $aut_nums[a][:haz][:ip4] == 0 and $aut_nums[a][:haz][:ip6] != 0}.size
lines << ["IPv6-only", ip6o, ip6o.to_f*100/($aut_nums.size)]
ip4o=$aut_nums.keys.select {|a| $aut_nums[a][:haz][:ip6] == 0 and $aut_nums[a][:haz][:ip4] != 0}.size
lines << ["IPv4-only", ip4o, ip4o.to_f*100/($aut_nums.size)]
dual=$aut_nums.keys.select {|a| ($aut_nums[a][:haz][:ip4] != 0) and ($aut_nums[a][:haz][:ip6] != 0)}.size
lines << ["Dualstack", dual, dual.to_f*100/($aut_nums.size)]
noip=$aut_nums.keys.select {|a| ($aut_nums[a][:haz][:ip4] == 0) and ($aut_nums[a][:haz][:ip6] == 0)}.size
lines << ["No IP", noip, noip.to_f*100/($aut_nums.size)]
lines << ["Total", $aut_nums.size, '']

pretty_print(lines)

## print a few free ASN's
puts
puts "Unallocated ASNs:"
# find the root blocks
roots=$as_blocks.keys.reject {|key| $as_blocks[key].include?(:parent)}

# now select 20 random numbers for each root block
$free_asn=[]
roots.each do |block|
  list=[]
  loop do
    # randomly generate ASN
    asn=block.begin+Random::rand(block.end - block.begin)
    
    # skip non-free ASN's
    next if $aut_nums.include?(asn)
    
    # find most specific 
    msb=$as_blocks.keys.select {|block| block.include?(asn)}.reject {|block| $as_blocks[block][:childs] and ($as_blocks[block][:childs].select{|cblock| cblock.include?(asn)}.length > 0)}[0]
    next unless msb
    msbe=$as_blocks[msb][:entry]
    
    # skip those with closed peering policy
    next if (msbe.props[:policy][0].nil? or (msbe.props[:policy][0].strip != 'open'))
    
    list << asn
    break if list.length > 19
  end
  $free_asn.concat(list)
end

lines=[]
$free_asn.sort.each_slice(5) do |line|
  lines << line.map(&:to_i)
end
pretty_print(lines)
