#!/usr/bin/env ruby

require 'rubygems'
require 'find'
require 'netaddr'

DATA='../data/inetnum'

class NetAddr::CIDR
  def hash
    self.to_i(:ip) ^ (self.bits * self.all_f / self.address_len) ^ self.tag.hash
  end
end

# range => [ns]
RANGES={}

if ARGV[0].nil?
  puts "Usage:   ruby ptrgen.rb <CIDR>"
  puts "Example: ruby ptrgen.rb 172.22.0.0/16"
  exit
end

# utility function to load some whois file
def load_file(f)
  File::read(f).split("\n").map{|l| l.strip}.reject{|l| l.length==0}.map{|l|l.split(" ", 2).map{|p|p.strip}}.reject{|p| p.length != 2}.map{|p| [p[0][0..-2].to_sym, p[1]]}.inject(Hash.new {|h,k| h[k]=[]}) {|h,p| h[p[0]] << p[1];h}
end

# range we should generate rdns for
RANGE=NetAddr::CIDR.create(ARGV[0])
if RANGE.bits % 8 != 0
    puts "Given subnet #{RANGE} has no netmask dividable by 8."
    puts "Zones can only be generated for /8, /16, /24 and /32!"
    exit
end

# ip -> name
NS_MAP={}

Find.find(DATA) do |f|
  next unless File::file?(f)
  next if (f.end_with?("~") or f.start_with?("~"))
  rangeS=File::basename(f).split("_")
  range=NetAddr::CIDR.create(rangeS.join('/'))
  next unless RANGE.matches?(range)
  ns=[]
  data=load_file(f)
  data[:nserver].each do |nserver|
    srv,$_=nserver.strip.split(' ')
    begin
      srv_ip=NetAddr::CIDR.create(srv)
      name=srv_ip.arpa.gsub('in-addr.arpa.', 'ip4.').gsub('arpa.', '') + "srv.#{RANGE.arpa[0..-2]}"
      NS_MAP[srv_ip]=name
      ns << name
    rescue NetAddr::ValidationError
      ns << srv
    end
  end
  RANGES[range]=ns unless ns.empty?
end

puts "$ORIGIN ."
puts "$TTL 2d"
puts "#{RANGE.arpa} IN SOA nic.dn42. root.nic.dn42. ("
puts "    #{Time.now.utc.to_i} ; serial"
puts "    4h ; refresh"
puts "    1h ; retry"
puts "    2w ; expire"
puts "    2d ; minimum"
puts ")"
puts ""

result = {}

# iterate over all assigned subnets, no matter what size, and assign nameserver to all next smaller /16, /24 or /32.
# nameservers of smaller subnets are preferred.
RANGES.keys.each do |p|
  if not p.nil? and RANGE.contains?(p) or RANGE == p or p.contains?(RANGE)
    if p.bits % 8 == 0
      result[p] = p
    else
      p.subnet(:Bits => (p.bits / 8 + 1) * 8, :Objectify => true).each do |range|
        result[range] = p if ( RANGE.contains?(range) or RANGE == range) and (not (result.has_key?(range)) or result[range].nil? or result[range].bits < p.bits)
      end
    end
  end
end


NetAddr::sort(result.keys).each do |range|
  matches=RANGES[result[range]]
  next unless matches.length > 0
  matches.sort.each do |entry|
    puts "#{range.arpa}\tNS\t#{entry}."
  end
end

# dump all records with an ip address..
NS_MAP.each_key do |ip|
  if ip.bits == 32
    puts "#{NS_MAP[ip]}.\tA\t#{ip.ip}"
  else
    puts "#{NS_MAP[ip]}.\tAAAA\t#{ip.ip(:Short => true)}"
  end
end
