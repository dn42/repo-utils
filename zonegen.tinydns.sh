#!/bin/sh
echo '.dn42:172.22.53.51:a.dn42ns.crazydns.dn42'
cat data/dns/*.dn42 | awk '/^domain:/ { d = $2 } /^nserver:/ { print "&" d ":" $3 ":" $2 }'
