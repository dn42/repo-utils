#!/usr/bin/env ruby

require 'rubygems'
require 'find'
require 'netaddr'

DATA='../data/inetnum'

# range => [ns]
RANGES={}

if ARGV[0].nil?
  puts "Usage:   ruby ptrgen.rb <CIDR>"
  puts "Example: ruby ptrgen.rb 172.22.255.0/24"
  exit
end

# range we should generate rdns for
RANGE=NetAddr::CIDR.create(ARGV[0])

Find.find(DATA) do |f|
  next unless File::file?(f)
  next if (f.end_with?("~") or f.start_with?("~"))
  rangeS=File::basename(f).split("_")
  range=NetAddr::CIDR.create(rangeS.join('/'))
  next unless RANGE.matches?(range)
  ns=[]
  File::read(f).split("\n").each do |line|
    case line
    when /^nserver:\s+(.+)$/
      ns << $1
    end
  end
  RANGES[range]=ns unless ns.empty?
end

# origin for all records
puts "$ORIGIN #{RANGE.arpa}"

# output all NS records first
RANGES.each do |range, ns|
  ns.each do |n|
    puts "#{range.network.split('.')[-1]}#{range.netmask}\tNS\t#{n}."
  end
end

# now iterate over all IP's of the given subnet
RANGE.subnet(:Bits => 32, :Objectify => true).each do |ip|
  matches=[]
  RANGES.keys.each do |p|
    matches << p if p.contains?(ip)
  end
  matches = NetAddr.sort(matches)
  next unless matches.length > 0
  match=matches[-1]
  host_last_octet=ip.arpa.split('.', 2)[0]
  net_last_octet=match.network.split('.')[-1]
  puts "#{host_last_octet}\tCNAME\t#{host_last_octet}.#{net_last_octet}#{match.netmask}"
end
