#!/usr/bin/env ruby

require 'rubygems'
require 'netaddr'
require 'open-uri'
require 'net/https'
require 'find'

# fetch current BGP Filter blah from wiki in text form
entries_wiki={}
open('https://dn42.net/trac/wiki/IPv4Topologie?format=txt', :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE) do |f|
  last_comment=''
  active=false
  f.each do |line|
    case line
    when /^\s*\|\|(.*)/
      parts=$1.split("||").map {|a| a.strip}
      unless active
        active=true if parts[0]=='MARKER-SPECIAL:dn42-start'
      else
        case parts[0]
        when /^(\d+\.){3}\d+\/\d+/
          # simple... $ipaddress
        when /^\[wiki:\s*((\d+\.){3}\d+\/\d+).*\]?/
          parts[0]=$1
          # simple.. wiki link $ipaddress blah..
        when "MARKER-SPECIAL:dn42-end"
          active=false
          next
        else
          # something else
          next
        end
        addr=NetAddr::CIDR.create(parts[0])
        entries_wiki[addr]={
          :status => parts[1],
          :ml_subscription => parts[2],
          :user => parts[3]
        }
      end
    end
  end
end

def load_dir(name)
  ret={}
  Find::find("../data/#{name}") do |f|
    next unless File::file?(f)
    next if (f.end_with?("~") or f.start_with?("~"))
    a=NetAddr::CIDR.create(File::basename(f).split('_').join('/'))
    ret[a]={
      :file => f
    }
  end
  return ret
end

# load registry data
entries_inetnum=load_dir('inetnum')
entries_route=load_dir('route')

# returns an hash with bits => count
def count_prefixes(list)
  ret=[]
  list.map {|prefix| prefix.bits}.sort.inject(Hash.new(0)) {|hash,bits| hash[bits]=hash[bits]+1; hash}
end

count_inetnum=count_prefixes(entries_inetnum.keys)
count_route=count_prefixes(entries_route.keys)
count_wiki=count_prefixes(entries_wiki.keys.reject {|prefix| a=entries_wiki[prefix][:status]; a.nil? or not a.include?('active')})

# now build lines with  [bits, inetnum, route, wiki]
counts=[]
(1..32).each do |bits|
  line=[bits]
  match=false
  [count_inetnum,count_route,count_wiki].each do |count|
    line << count[bits]
    match=true if count[bits] != 0
  end
  counts << line if match
end


puts "Entry summary:"
puts "\tPrefix length\t\tinetnum\t\troute\t\tWiki"
counts.each do |line|
  puts "\t\t/#{line[0]}\t\t#{line[1]}\t\t#{line[2]}\t\t#{line[3]}"
end
puts "\t\tTotal:\t\t#{entries_inetnum.keys.size}\t\t#{entries_route.keys.size}\t\t#{entries_wiki.keys.reject {|prefix| a=entries_wiki[prefix][:status]; a.nil? or not a.include?('active')}.size}"
puts
