#!/usr/bin/env python2

# By: NeoRaider


import sys, os, socket, string, ipaddr


if len(sys.argv) != 2:
    sys.stderr.write('Usage: %s bird6.conf.in > bird6.conf\n\n\tThe script will replace all occurrences of @BIRD6GEN@ with generated filter rules\n' % sys.argv[0])
    sys.exit(1)

routedir = os.path.dirname(sys.argv[0]) + '/../../data/route6'


networks = []

for name in os.listdir(routedir):
    try:
        with open(routedir+'/'+name) as f:
            for line in f:
                if not line.startswith('route6:'):
                    continue

                entry = line.strip().split()[1]
                try:
                    network = ipaddr.IPv6Network(entry, True)
                except:
                    network = ipaddr.IPv6Network(entry).masked()
                    sys.stderr.write('Warning: Invalid network %s; using %s instead.\n' % (entry, network))

                if network.prefixlen > 64:
                    sys.stderr.write('Warning: Prefix lengths >64 aren\'t supported; ignoring network %s.\n' % network)
                else:
                    networks.append(network)

                break
    except:
        sys.stderr.write('Warning: Unable to parse file \'%s\'; ignoring this file\n' % name)

outstring = ''

for network in networks:
    outstring += ('%(prefix)s/%(len)i{%(len)i,64},\n' % {'prefix': network.network, 'len': network.prefixlen})

try:
    f = open(sys.argv[1])
    data = f.read()
    f.close()
except:
    sys.stderr.write('Error: Unable to read input file \'%s\'.\n' % sys.argv[1])
    sys.exit(1)


sys.stdout.write(string.replace(data, '@BIRD6GEN@', outstring))
