#!/usr/bin/env python2

# By: NeoRaider


import sys, os, socket, string, ipaddr

routedir = os.path.join(os.path.dirname(__file__),  '../../data/route6')


networks = []

for name in os.listdir(routedir):
    try:
        with open(routedir+'/'+name) as f:
            for line in f:
                if not line.startswith('route6:'):
                    continue

                entry = line.strip().split()[1]
                try:
                    network = ipaddr.IPv6Network(entry, True)
                except:
                    network = ipaddr.IPv6Network(entry).masked()
                    sys.stderr.write('Warning: Invalid network %s; using %s instead.\n' % (entry, network))

                if network.prefixlen > 64:
                    sys.stderr.write('Warning: Prefix lengths >64 aren\'t supported; ignoring network %s.\n' % network)
                else:
                    networks.append(network)

                break
    except:
        sys.stderr.write('Warning: Unable to parse file \'%s\'; ignoring this file\n' % name)

outstring = ''
outstring += 'function match_valid_network()\n'
outstring += 'prefix set nets;\n'
outstring += '{\n'
outstring += '\tnets=[\n'
for network in networks:
    outstring += ('\t\t%(prefix)s/%(len)i{%(len)i,64},\n' % {'prefix': network.network, 'len': network.prefixlen})
outstring += '\t];\n'
outstring += '\treturn net ~ nets;\n'
outstring += '}\n'


sys.stdout.write(outstring)
