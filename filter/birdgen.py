#!/usr/bin/env python2

# By: NeoRaider


import sys, os, socket, string, ipaddr


if len(sys.argv) != 2:
    sys.stderr.write('Usage: %s bird.conf.in > bird.conf\n\n\tThe script will replace all occurrences of @BIRDGEN@ with generated filter rules\n' % sys.argv[0])
    sys.exit(1)

routedir = os.path.dirname(sys.argv[0]) + '/../../data/route'


networks = [ipaddr.IPv4Network('172.22.0.0/15')]
smallnetworks = []

for name in os.listdir(routedir):
    try:
        with open(routedir+'/'+name) as f:
            for line in f:
                if not line.startswith('route:'):
                    continue

                entry = line.strip().split()[1]
                try:
                    network = ipaddr.IPv4Network(entry, True)
                except:
                    network = ipaddr.IPv4Network(entry).masked()
                    sys.stderr.write('Warning: Invalid network %s; using %s instead.\n' % (entry, network))

                if network.prefixlen > 28:
                    smallnetworks.append(network)
                else:
                    networks.append(network)

                break
    except:
        sys.stderr.write('Warning: Unable to parse file \'%s\'; ignoring this file\n' % name)

outstring = ''

networks = ipaddr.collapse_address_list(networks)

for network in networks:
    outstring += ('%(prefix)s/%(len)i{%(len)i,28},\n' % {'prefix': network.network, 'len': network.prefixlen})

for network in smallnetworks:
    outstring += ('%s,\n' % network.with_prefixlen)

try:
    f = open(sys.argv[1])
    data = f.read()
    f.close()
except:
    sys.stderr.write('Error: Unable to read input file \'%s\'.\n' % sys.argv[1])
    sys.exit(1)


sys.stdout.write(string.replace(data, '@BIRDGEN@', outstring))
