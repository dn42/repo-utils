#!/usr/bin/env ruby

require 'find'




# utility function to load some whois file
def load_file(f)
  File::read(f).split("\n").map{|l| l.strip}.reject{|l| l.length==0}.map{|l|l.split(" ", 2).map{|p|p.strip}}.reject{|p| p.length != 2}.map{|p| [p[0][0..-2].to_sym, p[1]]}.inject(Hash.new {|h,k| h[k]=[]}) {|h,p| h[p[0]] << p[1];h}
end

# NS=>IP mapping
$glues=Hash.new {|h,k| h[k]=[]}

# zone=>NS entries
$entries=Hash.new {|h,k| h[k]=[]}

DATA='../data/dns'

puts "$ORIGIN .
$TTL 172800\t; 2 days
dn42\tIN\tSOA\tnic.dn42. root.nic.dn42. (
\t\t\t#{Time.now.utc.to_i}\t; serial
\t\t\t14400\t\t; refresh (4 hours)
\t\t\t3600\t\t; retry (1 hour)
\t\t\t1209600\t\t; expire (2 weeks)
\t\t\t172800\t\t; minimum (2 days)
\t\t\t)
"

Find.find(DATA) do |f|
  # skip a few files... like backup files or directories? or the root-zone
  next unless File::file?(f)
  next if (f.end_with?("~") or f.start_with?("~"))
  
  # get the file name
  zone=File::basename(f)
  
  # load the file
  data=load_file(f)
  
  # iterate over the nserver entries
  data[:nserver].each do |entry|
    ns,glue=entry.split(' ')
    $entries[zone] << ns
    $glues[ns] << glue if glue
  end
end

# find maximum size
$max_zone=0
$entries.keys.each do |ns|
  $max_zone=ns.length if ns.length > $max_zone
end
$glues.keys.each do |ip|
  $max_zone=ip.length if ip.length > $max_zone
end

# write dn42 zone
$entries.keys.sort.each do |zone|
  $entries[zone].each do |ns|
    puts "#{zone}.#{' '*($max_zone+1-zone.length)}\tNS\t#{ns}."
  end
end

puts
puts ";;;;;;;;;;;;;;;;;;;;; glue records"

# write glue
$glues.keys.each do |glue|
  next unless glue.end_with? ".dn42"
  $glues[glue].uniq.each do |ip|
    print "#{glue}.#{' '*($max_zone-glue.length)}\t"
    print 'A' if ip.index(':').nil?
    print 'AAAA' unless ip.index(':').nil?
    puts "\t#{ip}"
  end
end
