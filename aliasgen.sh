#!/bin/sh
# Generate mail aliases for various dn42 domains
#   by: stv0g@0l.de
#
# Posfix main.cf:
#
#   virtual_alias_domains = dn42, dn42.org, dn42.net, dn42.us, dn42.eu
#   virtual_alias_maps = regexp:/path/to/dn42_aliases

for FILE in ../data/person/*
do
	LOCAL=$(sed -ne 's/nic-hdl:\s*\([A-Za-z0-9.-]*\)-DN42/\L\1/p' $FILE)
	DESTS=$(sed -rne 's/(e-mail:|contact:\s*e?mail(to)?:)\s*(.*)/\3/p' $FILE | uniq)

	if [ -n "$DESTS" -a -n "$LOCAL" ]; then
			for DEST in $DESTS
			do
				echo "/$LOCAL@dn42(.org|.net|.us|.eu)?/ $DEST" 
			done
	fi
done
