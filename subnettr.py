#!/usr/bin/env python3

import csv,hashlib,os,time

#### Subnettr by Xuu <jon@xuu.cc>
#
#  Subnettr is a script for generating dns records suitable for use in BIND9. 
#  It will attempt to create delegate as much as possible to smaller subnets for reverse dns.
#  It does this by using RFC2317 CNAME records. 
#
#  The following script is used to generate the input csv files that the script consumes. 
#  In this example i have a symlink to the data directory of the mtn repo. 
#
#  Modify to your own purposes. 
#
# ~~~
#  #!/bin/bash
#  cd ~/ddns/data
#  mtn sync mtn://mtn.xuu.dn42
#  mtn update
#  REVISION=`mtn status|grep Revision|cut -d ' ' -f 2`
#
#  cd ~/ddns/
#  grep nserver data/inet6num/* | sed -E 's_:nserver:[\ \t]+_,_' | sed -E 's_[\ \t]+_,_' | sed s/_/,/ | sed s_data/inet6num/__ > inet6.txt
#  grep nserver data/inetnum/* | sed -E 's_:nserver:[\ \t]+_,_' | sed -E 's_[\ \t]+_,_' | sed s/_/,/ | sed s_data/inetnum/__ > inet.txt
#  grep nserver data/dns/* | sed -E 's_:nserver:[\ \t]+_,_' | sed -E 's_[\ \t]+_,_' | sed s/_/,/ | sed s_data/dns/__ > dns.txt
#
#  export SUBNETTR_CONTACT=xuu.dn42.us
#  export SUBNETTR_PERSON=XUU-DN42
#  export SUBNETTR_PRIMARY='souris.root.dn42'
#  export SUBNETTR_REVISION=$REVISION
#
#  ./subnettr.py
#
#  sudo service bind9 reload
# ~~~
#
####

contact=os.getenv('SUBNETTR_CONTACT', 'dummy.example.com')
primary=os.getenv('SUBNETTR_PRIMARY', 'dummy.root.dn42')
person=os.getenv('SUBNETTR_PERSON', 'DUMMY-DN42')
revision=os.getenv('SUBNETTR_REVISION', '0000000000000000000000000000000000000000')

UTC_DT = int(time.time())

def is_ipv4(address):
    parts = address.split(".")
    if len(parts) != 4:
        return False
    for item in parts:
        try:
          i = int(item)
        except ValueError:
          return False
        if not 0 <= i <= 255:
            return False
    return True

def is_ipv6(address):
   has_empty = False
   num_part = 0
   last = None
   for i in address.lower():
     if i in '0123456789abcdef': 
       pass
     elif i == ':':
       if last == ':':
         if has_empty:
           return False
         has_empty = True
     else: 
       return False
     last = i
   return True

def toNum (ip):
  ip = [int(i) for i in ip.split('.')]
  return ip[3] + ip[2] * 256 + ip[1] * 256**2 + ip[0] * 256**3 

def toIP (num):
  return num >> 24, (num >> 16)&0xFF, (num >> 8)&0xFF, num&0xFF

def print_header(file, time, contact, primary, person, f):
  print('; File: db.' + file, file=f)
  print('$TTL 300', file=f)
  print('{file}. IN SOA {primary}. {contact}. ({time} 14400 3600 1209600 300)'.format(file=file, time=time, contact=contact, primary=primary), file=f)
  print('@ IN TXT {0},person={1},rev={2},ts={3}'.format(primary, person, revision, UTC_DT), file=f)
  print('_person._info IN TXT {0}'.format(person), file=f)
  print('_contact._info IN TXT {0}'.format(contact), file=f)
  print('_rev._info IN TXT {0}'.format(revision), file=f)
  print('_ts._info IN TXT {0}'.format(UTC_DT), file=f)
  print('_primary._info IN CNAME {0}'.format(primary), file=f)

def print_glue(ns, addr, f):
    if is_ipv6(addr):
      print('{0} IN AAAA {1}'.format(ns, addr), file=f)
    elif is_ipv4(addr):
      print('{0} IN A {1}'.format(ns, addr), file=f)

def print_ns(ns, addr, f):
      if is_ipv6(ns):
        print('{0} IN NS {1} ; Invalid Record'.format(addr, dummy(ns)), file=f)
        print_glue(dummy(ns), ns, f)
          
      elif is_ipv4(ns):
        print('{0} IN NS {1} ; Invalid Record'.format(addr, dummy(ns)), file=f)
        print_glue(dummy(ns), ns, f)

      else:
        print('{0} IN NS {1}'.format(addr, ns), file=f)

def dummy(addr):
  return 'DUMMY'+hashlib.sha1(addr.encode()).hexdigest()[:8]


# Build the reverse dns with subnet delegation.

class InetTree:
  """Inet Tree Walker"""

  root = None

  def add(self, addr, mask, val=None):

    inet = toNum(addr) & (0xFFFFFFFF << 32 - mask)
    if self.root == None: self.root = {'inet':0, 'val':None, 'left':None, 'right':None} 
    curr = self.root
    
    def it(inet, mask):
      m = 0x80000000
      n = 0x80000000

      for i in range(0, mask):
        yield 'right' if inet&m > 0 else 'left', inet&n
        m = m >> 1
        n = (n >> 1) | 0x80000000

    for i, n in it(inet, mask):
        if curr[i] == None: curr[i] = {'inet':n, 'val':None, 'left':None, 'right':None} 
        curr = curr[i]

    curr['inet'] = inet
    curr['val'] = val

  def walk(self):
    def re(node, lvl):
      if node['inet']>>24 < 127 and lvl == 8:
        yield (node['inet'], lvl, node['val'])
      elif node['inet']>>24 < 192 and lvl == 16:
        yield (node['inet'], lvl, node['val'])
      elif node['inet']>>24 >= 192 and lvl == 24:
        yield (node['inet'], lvl, node['val'])

      # Leaf Node
      if node['left'] == None and node['right'] == None: 
        yield (node['inet'], lvl, node['val'])
        return
      # Branch Left  
      elif node['left'] != None and node['right'] == None:
        if node['left']['val'] == None: node['left']['val'] = node['val']
        for i in re(node['left'], lvl+1):
          yield i
        yield (node['inet']|0x80000000>>lvl, lvl+1, node['val'])
        return
      # Branch Right  
      elif node['left'] == None and node['right'] != None:
        if node['right']['val'] == None: node['right']['val'] = node['val']
        yield (node['inet'], lvl+1, node['val'])
        for i in re(node['right'], lvl+1):
          yield i
        return
      # Branch Both
      else:  
        if node['left']['val'] == None: node['left']['val'] = node['val']
        if node['right']['val'] == None: node['right']['val'] = node['val']
        for i in re(node['left'], lvl+1):
          yield i
        for i in re(node['right'], lvl+1):
          yield i
    # Some installations don't yet have yield from :-(      
    for i in re(self.root, 0):
      if i[2] != None:
        yield i

DNS = {}
INET = {}
INETtree = InetTree()

with open('inet.txt') as f:
  def get_file(s):
    if s[0] < 128:
      file = str(s[0])
    elif s[0] < 192:
      file ='.'.join([str(i) for i in s[:2]][::-1])
    else:
      file ='.'.join([str(i) for i in s[:3]][::-1])
    return file+'.in-addr.arpa'

  r=csv.reader(f)
  for line in r:
    inet=line[0] 
    mask=int(line[1])
    dns=(tuple([int(i) for i in inet.split('.')]), mask)

    if dns not in DNS: 
      INETtree.add(inet, mask, dns)
      DNS[dns] = []

    if is_ipv4(line[2]) or is_ipv6(line[2]):
      pass
    else:
      line[2] = line[2].strip('.')+'.'
        
    DNS[dns].append(line)

# Networks
for num, mask, dns in INETtree.walk():
    file=get_file((num >> 24, (num >> 16)&0xFF, (num >> 8)&0xFF, num&0xFF))
    if file not in INET: INET[file] = []
    INET[file].append((num, mask, dns))

for file, data in INET.items():
  with open('db.'+file,'w') as f:
    print_header(file, UTC_DT, contact, primary, person, f)
    for num, mask, dns in data:
      ip=toIP(num)
      size=mask // 8
      num=2**(8-(mask % 8))

      print('\n;; inetnum %s/%d' % ('.'.join([str(i) for i in ip]), mask), file=f)
      print('$ORIGIN '+'.'.join([str(i) for i in ip[:size]][::-1]) + '.in-addr.arpa.', file=f)
      if mask%8 == 0: 
        for ns in DNS[dns]: print_ns(ns[2], '@', f)
      elif mask < 24:
        for ns in DNS[dns]: print_ns(ns[2], '$GENERATE {start}-{end} $'.format(start=ip[size], end=int(ip[size])+num-1), f)
      else: 
        print('$GENERATE {start}-{end} $ CNAME $.{start}/{mask}'.format(start=ip[size], end=int(ip[size])+num-1, mask=mask), file=f)
        for ns in DNS[dns]: print_ns(ns[2], str(ip[size]) + '/' + str(mask), f)

# Build the v6 reverse dns
DNS = {} 
INET6 = {}

with open('inet6.txt') as f:
  def expand_ipv6(addr):
    if "::" in addr:
        addr = addr.replace('::', ':' * (9 - addr.count(':')))
    if addr.count(':') != 7:
        return False
    return ''.join((i.zfill(4) for i in addr.split(":")))

  r = csv.reader(f)
  for l in r:
    inet = tuple([i for i in expand_ipv6(l[0]).rstrip('0')])
    file='.'.join(inet[:2][::-1]) + '.ip6.arpa'

    d = l[2]
    if is_ipv4(d) or is_ipv6(d):
      pass
    else:
      d = d+'.'

    if file not in INET6:
      INET6[file] = {}

    if inet not in INET6[file]:
      INET6[file][inet] = set()

    INET6[file][inet].add(d)

for file, inet in INET6.items():
  with open('db.'+file,'w') as f:
    print_header(file, UTC_DT, contact, primary, person, f)
    for k, v in inet.items():
      for ns in v:
        print_ns(ns, '.'.join(k[::-1]) + '.ip6.arpa.', f)


# Build the regular DNS
DNS = {} 
DOMAIN = {}
DOMAIN['root.dn42'] = { 'root.dn42.': set() }

with open('dns.txt') as f:
  r = csv.reader(f)
  for l in r:
    file =  l[0].split('.')[-1]
    d = l[1]
    if is_ipv4(d) or is_ipv6(d):
      pass
    else:
      d = d+'.'

    o = l[0]+'.' 

    if file not in DOMAIN:
      DOMAIN[file] = {}

    if o not in DOMAIN[file]:
      DOMAIN[file][o] = set()

    DOMAIN[file][o].add(d)

    if o == 'dn42.': DOMAIN['root.dn42']['root.dn42.'].add(d)

    if d not in DNS:
      DNS[d] = set()

    if len(l) > 2 and l[2] != '':
      DNS[d].add(l[2])

for file, d in DOMAIN.items():
  with open('db.'+file,'w') as f:
    print_header(file, UTC_DT, contact, primary, person, f)
    for k, v in d.items():
      for ns in v: 
        print_ns(ns, k,  f)
        for i in DNS[ns]:
          print_glue(ns, i, f)
