#!/bin/sh
echo '.22.172.in-addr.arpa:172.22.53.52:a.rdns.crazydns.dn42'
echo "+a.smallblocksns.crazydns.dn42:172.22.53.54"
for temp1 in data/inetnum/*_* ; do
 if [ "$temp1" != data/inetnum/172.22.254.0_24 ]; then
  temp2="$(basename $temp1)"
  ip="$(echo "$temp2" | cut -d_ -f1)"
  len="$(echo "$temp2" | cut -d_ -f2)"
  cidr="$ip/$len"
  if [ "$len" -gt 24 ]; then
   newcovering24="$(./utils/cidr2 /24s "$cidr")"
   if [ "x$covering24" = x ]; then
    covering24="$newcovering24"
   elif [ "$newcovering24" != "$covering24" ]; then
    echo "&$(./utils/cidr2 ptrzone "$covering24")::a.smallblocksns.crazydns.dn42"
    covering24="$newcovering24"
   fi
  elif [ "$len" -lt 22 ]; then
   true
  else
   for tempcidr in $(./utils/cidr2 /24s "$cidr") ; do
    tempcidrzone="$(./utils/cidr2 ptrzone "$tempcidr")"
    cat $temp1 | awk '/^nserver:/ { print "&" "'"$tempcidrzone"'" ":" $3 ":" $2 }'
   done
  fi
 fi
done
if [ "x$covering24" != x ]; then
 echo "&$(./utils/cidr2 ptrzone "$covering24")::a.smallblocksns.crazydns.dn42"
fi
