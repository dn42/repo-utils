#!/usr/bin/env ruby

require 'rubygems'
require 'netaddr'
require 'open-uri'
require 'net/https'
require 'find'

# fetch current BGP Filter blah from wiki in text form
entries_wiki=[]
comments=Hash.new('')
open('https://dn42.net/trac/wiki/BGP_Filter?format=txt', :ssl_verify_mode => OpenSSL::SSL::VERIFY_NONE) do |f|
  last_comment=''
  f.each do |line|
    case line
      when /ipv6 prefix-list .* permit (.*)/
      a=NetAddr::CIDR.create($1.split(" ")[0])
      entries_wiki << a
      comments[a]=last_comment
      when /!(.*)/
      last_comment=$1
    end
  end
end


def load_dir(name)
  ret=[]
  names={}
  Find::find("../data/#{name}") do |f|
    next unless File::file?(f)
    next if (f.end_with?("~") or f.start_with?("~"))
    a=NetAddr::CIDR.create(File::basename(f).split('_').join('/'))
    ret << a
    names[a]=f
  end
  return ret,names
end

# fetch registered networks from registry
entries_route6,_=load_dir('route6')
entries_inet6num,files_inet6num=load_dir('inet6num')

# returns an hash with bits => count
def count_prefixes(list)
  ret=[]
  list.map {|prefix| prefix.bits}.sort.inject(Hash.new(0)) {|hash,bits| hash[bits]=hash[bits]+1; hash}
end

count_inet6num=count_prefixes(entries_inet6num)
count_route6=count_prefixes(entries_route6)
count_wiki=count_prefixes(entries_wiki)

# now build lines with  [bits, inet6num, route6, wiki]
counts=[]
(1..128).each do |bits|
  line=[bits]
  match=false
  [count_inet6num,count_route6,count_wiki].each do |count|
    line << count[bits]
    match=true if count[bits] != 0
  end
  counts << line if match
end

if ARGV[0]=='news'
  puts "From: ipv6-reports@welterde.de"
  puts "Newsgroups: alt.net.dn42.users"
  puts "Subject: IPv6 registry report"
  puts "Date: #{Time.now.rfc2822}"
  puts
else
  puts "IPv6 Prefix report"
  puts "=================="
end
puts "Entry summary:"
puts "\tPrefix length\t\tinet6num\troute6\t\tWiki BGP_Filter"
counts.each do |line|
  puts "\t\t/#{line[0]}\t\t#{line[1]}\t\t#{line[2]}\t\t#{line[3]}"
end
puts "\t\tTotal:\t\t#{entries_inet6num.size}\t\t#{entries_route6.size}\t\t#{entries_wiki.size}"
puts

# check for route6 objects without inet6num object
puts "Registry route6 objects without matching inet6num object:"
entries_route6.each do |route|
  match=false
  entries_inet6num.each do |inetnum|
    match=true if (inetnum.contains?(route) or inetnum == route)
  end
  puts "\t#{route.to_s(:Short => true)}" unless match
end
puts

# now match registry routes against bgp filter
puts "Registry route6 prefixes not matched by BGP filter in the wiki:"
entries_route6.each do |prefix|
  match=false
  entries_wiki.each do |filter|
    match=true if (filter.contains?(prefix) or filter == prefix)
  end
  unless match
    # find matching inet6num object
    matches=[]
    entries_inet6num.each do |inetnum|
      matches << inetnum if (inetnum.contains?(prefix) or inetnum == prefix)
    end
    matches.sort!
    # now find the netname
    name=""
    if matches.size > 0
      a=File.read(files_inet6num[matches[0]]).split("\n")
      a.each do |line|
        case line
        when /^netname:\s*(.*)$/
          name=$1
          break
        end
      end
    end
    a="\t\t" if prefix.bits <= 64
    a="\t" if prefix.bits > 64
    puts "\t#{prefix.to_s(:Short => true)}#{a}#{name}"
  end
end
puts

# now check vice versa, but only take into account filters that don't match an whole /7 or so like for ULA
puts "Wiki BGP filter prefixes probably missing in the registry:"
entries_wiki.each do |prefix|
  next if prefix.bits < 32
  match=false
  (entries_route6 + entries_inet6num).uniq.each do |reg|
    match=true if (prefix.contains?(reg) or reg == prefix)
  end
  a="\t\t" if prefix.bits <= 64
  a="\t" if prefix.bits > 64
  puts "\t#{prefix.to_s(:Short => true)}#{a}#{comments[prefix]}" unless match
end
puts

# output meta info
puts "Version:"
puts "\tRegistry revision:\t\t#{`mtn au get_base_revision_id`}"
puts "\tWiki date:        \t\t#{Time.now.to_s}"
puts
