#!/bin/sh
echo "\$TTL 172800"
echo "dn42. IN SOA nic.dn42. root.nic.dn42. (`date +%s` 14400 3600 1209600 172800)"
for file in ../data/dns/*dn42
do
	if [ -f "$file" ]
	then
		zone=`basename "$file"`
		echo "; $zone from $file"
		sed -n "s/^nserver:[ \t]\+\([^ \t]\+\)$/$zone. IN NS \1./p" $file
		sed -n "s/^nserver:[ \t]\+\([^ \t]\+\)[ \t].*$/$zone. IN NS \1./p" $file
		sed -n "s/^nserver:[ \t]\+\([^ \t]\+\)[ \t]\+\([0-9]\+\.[0-9.]\+\)[ \t]*$/\1. IN A \2/p" $file
		sed -n "s/^nserver:[ \t]\+\([^ \t]\+\)[ \t]\+\([0-9a-fA-F]*:[0-9a-fA-F:]\+\)[ \t]*$/\1. IN AAAA \2/p" $file
	fi
done
# CHANGED 2012-06-21 by XUU-DN42: changed the search to *dn42 from *.dn42 because it was missing the root nameservers in dn42 causing bind to error no NS records for zone. 
