#!/usr/bin/env ruby

require 'rubygems'
require 'find'

$: << '../lib'
require 'entry'
require 'load'

# load route dirs
routes4=DN42::load_dir("../data/route")
routes4.merge!(DN42::load_dir("../data/inetnum"))
routes6=DN42::load_dir("../data/route6")
routes6.merge!(DN42::load_dir("../data/inet6num"))

def process(routes)
  written=[]
  routes.keys.sort do |b,a|
    # reverse a and b to get most specific first
    z=a.bits <=> b.bits
    z=a.network <=> b.network if z==0
    z
  end.each do |route|
    next if written.include?(route)
    written << route
    asn=routes[route][:entry].props[:origin]
    if asn
      asn=asn[0]
      next unless asn
      puts "#{asn[2..-1].to_i},#{route}"
    else
      puts route
    end
  end
end
process(routes4)
process(routes6)

