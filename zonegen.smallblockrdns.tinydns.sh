#!/bin/sh
echo "+a.smallblockrdns.crazydns.dn42:172.22.53.54"
for temp1 in data/inetnum/*_* ; do
 if [ "$temp1" != data/inetnum/172.22.254.0_24 ]; then
  temp2="$(basename $temp1)"
  ip="$(echo "$temp2" | cut -d_ -f1)"
  len="$(echo "$temp2" | cut -d_ -f2)"
  cidr="$ip/$len"
  if [ "$len" -gt 24 ]; then
   thiszone="$(./utils/cidr2 ptrzone "$cidr")"
   if [ "x$thiszone" != "x$lastzone" ]; then
    echo ".$thiszone::a.smallblockrdns.crazydns.dn42"
    lastzone="$thiszone"
   fi
   cat "$temp1" | awk '/^nserver:/ { print "'"&$(./utils/cidr2 rfc2317zone "$cidr")"':" $3 ":" $2 }'
   ./utils/cidr2 rfc2317ptrs "$cidr" | for ptr in $(./utils/cidr2 ptrs "$cidr"); do
    read rfc2317ptr
    echo "C$ptr:$rfc2317ptr"
   done
  fi
 fi
done
