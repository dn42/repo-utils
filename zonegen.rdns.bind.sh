#!/bin/bash

OUTDIR="$3"

P=$0

NL="
"
print_error_and_exit ()
{
   echo "usage: $P   [inetnum directory]  [inetnum zone root file]  [zone output dir]"
   exit 1
}

parse_record ()
{
  ifss=$IFS
  DBUF=$(cat $1)
  IFS=$NL
  for op in $DBUF; do
    if echo $op | grep -P "^nserver:" >> /dev/null; then
      echo "$2     IN   NS      $(echo $op | awk '{print $2}')."
    fi
  done
  IFS=$ifss
  return 0;
}

gen_arpa ()
{
   DD=""; ic=0
   for c in `seq 0 3`; do
     ! echo ${ar2[$c+1]} | grep -P '[1-9]' > /dev/null && ic=1 
     DD="${ar2[$c]}.$DD"
     [ $ic -eq 1 ] && break
   done
   echo $DD"in-addr.arpa."
   return $c
}

gen_a ()
{
   gar=""
   for c in `seq 0 $[$1-1]`; do
     ! echo ${ar2[$c]} | grep -P '[1-9]' > /dev/null && break
     [ $c -gt 0 ] && gar=$gar"."
     gar=$gar${ar2[c]}
   done
   ! [ $2 -eq 2 ] && echo $gar
   return $[c]
}

gen_rev ()
{
   rmode=$2
   zsize=$3
   file=$1
   rbase=$(basename $1)
  
   echo $rootbase | grep $rbase && return 3
  
   set -- $(echo $rbase | tr '.' ' ' | tr '_' ' ')
   ar2=($1 $2 $3 $4 $5);

   O=$(echo $[$zsize-${ar2[4]}] | awk '{print 2^$1}')
 
   echo $O | grep "\." > /dev/null && echo -en "\n\r$1.$2.$3.$4/$5: invalid subnet size (min. /$zsize)\n" && return 2
    
   [ $O -eq 0 ] && O=$[O+1] 
       
   if [ $rmode -eq 2 ]; then 
     gen_a 4 2
     ROOTLEN=$?
     rootbase=$rbase
   fi
   
   for a in `seq 1 $O`; do
     RI=$(gen_a $ROOTLEN 0)
     FD="$OUTDIR/$RI.rev"
     arpa=$(gen_arpa $rbase);
     ar=$?
     ar2[$ar]=$[${ar2[$ar]}+1]

     if [ $rmode -eq 2 ]; then
	    ROOTS="$ROOTS $RI"
	    rm $FD
	    echo "\$ttl 172800$NL$arpa        IN   SOA     root.nixnodes.dn42. netadmin.eth.si. (`date +%s` 14400 3600 1209600 172800)" >> $FD
     elif [ $rmode -eq 1 ]; then
	    ! echo "$ROOTS" | grep "$RI" > /dev/null && echo -en "\n\r$1.$2.$3.$4/$5 out of range\n" && return 1
     fi
     parse_record $file $arpa >> $FD
   done
   
   return 0
}

ROOTLEN=0

SRC=$1

if [ -z "$SRC" ]; then
  print_error_and_exit
fi

ROOT=$2

if [ -z "$ROOT" ]; then
  print_error_and_exit
fi

if ! [ -d "$3" ]; then
  print_error_and_exit
fi

echo -en "processing: $(basename $ROOT)                                           \r"
gen_rev $ROOT 2 16

y=0; x=0;
for i in $SRC/*; do
 echo -en "processing: $(basename $i)                                           \r"
  gen_rev $i 1 24
  rr=$?
  [ $rr -eq 0 ] && y=$[y+1]
  x=$[x+1]
done
echo -en "processing: $y/$x OK                                           \n"
