#!/usr/bin/php
<?php
function readentries($dn) {
	$entries = array();
	$dir = opendir($dn);
	while (($item = readdir($dir)) !== FALSE) {
		if (!file_exists($dn.'/'.$item) || $item[0] == '.') continue;
		$entry = array();
		$contents = file_get_contents($dn.'/'.$item);
		$contents = explode("\n", $contents);
		foreach ($contents as $line) {
			if (!strlen($line)) continue;
			$parts = explode(':', $line, 2);
			$key = trim($parts[0]);
			$value = isset($parts[1])?trim($parts[1]):NULL;
			if (!array_key_exists($key, $entry)) {
				$entry[$key] = $value;
			} else if (is_array($entry[$key])) {
				$entry[$key][] = $value;
			} else {
				$entry[$key] = array($entry[$key], $value);
			}
		}
	        $entries[$item] = $entry;
	}
	closedir($dir);
//	ksort($entries);
	return $entries;
}

$entries = readentries('./data/dns');
$len1 = 25;
print('$TTL 172800'."\n");
print('dn42. IN SOA nic.dn42. root.nic.dn42. ('.time().' 14400 3600 1209600 172800)'."\n");
foreach ($entries as $key => $entry) {
	if (!is_array($entry['nserver'])) $entry['nserver'] = array($entry['nserver']);
	foreach ($entry['nserver'] as $ns) {
		$ns = str_replace("\t", ' ', $ns);
		$parts = explode(' ', $ns);
		print(str_pad($entry['domain'].'.', $len1).' IN NS   '.$parts[0].".\n");
		if (isset($parts[1]) && strlen($parts[1])) {
			if (strstr($parts[1], ':') === FALSE) {
				print(str_pad($parts[0].'.', $len1).' IN A    '.$parts[1]."\n");
			} else {
				print(str_pad($parts[0].'.', $len1).' IN AAAA '.$parts[1]."\n");
			}
		}
	}
}
