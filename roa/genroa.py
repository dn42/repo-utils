#!/usr/bin/env python
# (c) 2013 dsp
import os, sys, ipaddr, re
from operator import itemgetter
from optparse import OptionParser

usage  = "usage: %prog [options]"
desc   = "Generates bird compatible roa table commands for the dn42 network. \
The script assumes to be run inside the net.dn42.registry repository."

parser = OptionParser(usage=usage, epilog=desc)
parser.add_option("-q", "--quiet",
        action="store_false", dest="verbose", default=True,
        help="silently ignore invalid networks")
parser.add_option("-4", None, dest="ipv4", default=True,
        action="store_true", help="generate v4 roa tables")
parser.add_option("-6", None, dest="ipv6", default=False,
        action="store_true", help="generate v6 roa tables")
parser.add_option("-f", "--flush", dest="flush", default=False,
        action="store_true", help="print flush roa statement")
parser.add_option("-t", "--table", dest="table",
        help="roa table to use", metavar='T')
(options, args) = parser.parse_args()

if options.ipv6:
    routedir = os.path.join(os.path.dirname(sys.argv[0]), '../../data/route6')
elif options.ipv4:
    routedir = os.path.join(os.path.dirname(sys.argv[0]), '../../data/route')
else:
    print >> sys.stderr, 'Either v4 or v6 roa entries need to be generated.'
    sys.exit(-1)

networks = []
for route in os.listdir(routedir):
    with open(os.path.join(routedir, route)) as f:
        net = None
        asnum = None
        for line in f:
            if line.startswith('route6:'):
                e = line.strip().split()[1]
                try:
                    net = ipaddr.IPv6Network(e, True)
                except:
                    if options.verbose:
                        print >> sys.stderr , 'Invalid network %s for %s' % (net, e)
            elif line.startswith('route:'):
                e = line.strip().split()[1]
                try:
                    net = ipaddr.IPv4Network(e, True)
                except:
                    if options.verbose:
                        print >> sys.stderr , 'Invalid network %s for %s' % (net, e)
            elif line.startswith('origin:'):
                e = line.strip().split()[1]
                m = re.search('AS(\d+)', e)
                if m:
                    asnum = int(m.group(1))
        if asnum and net:
            networks.append((net, asnum))

table = options.table and ' table %s' % options.table or ''
if options.flush and len(networks) > 0:
    print 'flush roa%s' % table
for (n,asn) in sorted(networks, key=itemgetter(0)):
    print 'add roa %s max %d as %d%s' % (n, n.prefixlen, asn, table)
