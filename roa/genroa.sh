#!/bin/bash

for i in $*
do
	route=$(grep -E "route[6]?:" $i | tr -d '[:blank:]' | cut -d':' -f2-)
	as=$(grep "origin:" $i | tr -d '[:blank:][:alpha:]' | cut -d':' -f2)
	if [ -z "$as" -o -z "$route" -o -n "$(echo $as | tr -d '[:digit:]')" ]
	then
		echo "$i is invalid" >&2
		continue
	fi
	prefixlen=$(echo $route | cut -d'/' -f2)
	
	echo "add roa $route max $prefixlen as $as"
done
