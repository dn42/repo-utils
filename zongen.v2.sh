#!/bin/sh

# This script generate DNS zonefiles from the registry
# output are the zones
# - dn42.
# - 20.172.in-addr.arpa.
# - 22.172.in-addr.arpa.
# - 23.172.in-addr.arpa.
# - d.f.ip6.arpa.
#
# Features:
# - splitting IPv4 subnets for arpa zones (172.22.0.0/31 -> 172.22.0.0/32, 172.22.0.1/32)
#
# Author: Martin89 <martin89@martin89.de>
# license: cc-by-sa <https://creativecommons.org/licenses/by-sa/4.0/>
#
# Version: 1.2
#
# Changelog:
# 2014-09-06 version 1 released
# 2014-09-11 version 1.1: speed up based on parallelization
# 2015-10-10 version 1.2: little performance tweaks and support for 20.172.in-addr.arpa.
# ...
#

if [ ! -d "$1" ]; then
	echo 'The first argument musst be the directory with the dns information like "../data/dns"'
	exit 1
fi
DNS_DIR="$1"

if [ ! -d "$2" ]; then
	echo 'The 2. argument musst be the directory with the network information like "../data/inetnum"'
	exit 1
fi
INETNUM_DIR="$2"

if [ ! -d "$3" ]; then
	echo 'The 3. argument musst be the directory with the network information like "../data/inet6num"'
	exit 1
fi
INET6NUM_DIR="$3"

if [ ! -d "$4" ]; then
	echo 'get me a output directory'
	exit 1
fi
OUTPUT_DIR="$4"

if [ -z "$5" ]; then
	echo 'get me the Revision id'
	exit 1
fi
REV_ID="$4"

### Settings
primary='dns2.martin89.dn42'
contact='martin89.dn42'

# MAIN

FILE_DOMAIN="${OUTPUT_DIR}/domain.lst"
FILE_ARPA="${OUTPUT_DIR}/arpa.lst"
FILE_ARPA6="${OUTPUT_DIR}/arpa6.lst"

FILE_ARPA_ENTRYS="${OUTPUT_DIR}/entys_arpa.lst"
FILE_ARPA6_ENTRYS="${OUTPUT_DIR}/entys_arpa6.lst"
FILE_DOMAIN_ENTRYS="${OUTPUT_DIR}/entys_domain.lst"

FILE_ZONE_dn42="${OUTPUT_DIR}/dn42.zone"
FILE_ZONE_rev_20="${OUTPUT_DIR}/20.172.in-addr.arpa.zone"
FILE_ZONE_rev_22="${OUTPUT_DIR}/22.172.in-addr.arpa.zone"
FILE_ZONE_rev_23="${OUTPUT_DIR}/23.172.in-addr.arpa.zone"
FILE_ZONE_rev_fd="${OUTPUT_DIR}/d.f.ip6.arpa.zone"

## INENTNUM
{
	# seperate nserver information
	awk '{
			if ($1~ /^nserver:/ && $2!~ /[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ && $2!~ /[0-9A-Fa-f]+[:][0-9A-Fa-f:]+/) {
				net=FILENAME
				sub(".*/", "", net)
				domain=$2
				sub("\.$", "", domain)
				print net, domain, $3
			}
			}' "$INETNUM_DIR"/172.2[023].*_?? > "$FILE_ARPA"

	# build arpa entrys
	awk '{
		net = $1
		sub(".+_", "", net)
		cidr = net
		if (cidr == 8 || cidr == 16 || cidr == 24 || cidr == 32) { print $0 }
		else {
			sub("_.+", "", $1)
			network = $1
			split(network, BLOCK, "\.")
			if (int(cidr) <= 8) {
				for (i=BLOCK[1]; i <= 2^(8 - int(cidr)) - 1 + BLOCK[1]; i++) {
					print i "." 0 "." 0 "." 0 "_8", $2
				}
			}
			else {
				if (int(cidr) <= 16) {
					for (i=BLOCK[2]; i <= 2^(16 - int(cidr)) - 1 + BLOCK[2]; i++) {
						print BLOCK[1] "." i "." 0 "." 0 "_16", $2
					}
				}
				else {
					if (int(cidr) <= 24) {
						for (i=BLOCK[3]; i <= 2^(24 - int(cidr)) - 1 + BLOCK[3]; i++) {
							print BLOCK[1] "." BLOCK[2] "." i "." 0 "_24", $2
						}
					}
					else {
						for (i=BLOCK[4]; i <= 2^(32 - int(cidr)) - 1 + BLOCK[4]; i++) {
							print BLOCK[1] "." BLOCK[2] "." BLOCK[3] "." i "_32", $2
						}
					}
				}
			}
		}
	}' "$FILE_ARPA" | awk '{
		cidr = $1
		sub(".+_", "", cidr)
		network = $1
		sub("_.+", "", network)
		split(network, BLOCK, "\.")
		if (cidr == 32) {
			print BLOCK[4] "." BLOCK[3] "." BLOCK[2] "." BLOCK[1] ".in-addr.arpa.\tIN\tNS\t" $2 "."
		} else {
			if (cidr == 24) {
				print BLOCK[3] "." BLOCK[2] "." BLOCK[1] ".in-addr.arpa.\tIN\tNS\t" $2 "."
			} else {
				if (cidr == 16) {
					print BLOCK[2] "." BLOCK[1] ".in-addr.arpa.\tIN\tNS\t" $2 "."
				} else {
					print BLOCK[1] ".in-addr.arpa.\tIN\tNS\t" $2 "."
				}
			}
		}
	}' | sort -u > "$FILE_ARPA_ENTRYS"
} &

## INET6NUM
{
	# seperate nserver information
	awk '{
		if ($1~ /^nserver:/ && $2!~ /[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ && $2!~ /[0-9A-Fa-f]+[:][0-9A-Fa-f:]+/) {
				net=FILENAME
				sub(".*/", "", net)
				print net, $2, $3
		}
	}' "$INET6NUM_DIR"/fd*_?? > "$FILE_ARPA6"

	awk '
	function rev(str) {
		str=sprintf("%4s", str)
		gsub(" ", "0", str)
		x=""
		for (i=length(str); i != 0; i--) {
			x=x substr(str, i, 1);
		}
		return x
	}
	{
		cidr = $1
		sub(".+_", "", cidr)
		network = $1
		sub("_.+", "", network)
		split(network, BLOCK, "\:")
		if (cidr == 48) {
			entry=rev(BLOCK[3]) rev(BLOCK[2]) rev(BLOCK[1])
			gsub("", ".", entry)
			sub(".", "", entry)
			print entry "ip6.arpa.\tIN\tNS\t" $2 "."
		} else {
			if (cidr == 64) {
				entry=rev(BLOCK[4]) rev(BLOCK[3]) rev(BLOCK[2]) rev(BLOCK[1])
				gsub("", ".", entry)
				sub(".", "", entry)
				print entry "ip6.arpa.\tIN\tNS\t" $2 "."
			}
		}
	}' "$FILE_ARPA6" > "$FILE_ARPA6_ENTRYS"
} &

## DOMAIN
{
	awk '{
		if ($1~ /^nserver:/ && $2!~ /[0-9]+\.[0-9]+\.[0-9]+\.[0-9]+/ && $2!~ /[0-9A-Fa-f]+[:][0-9A-Fa-f:]+/) {
			domain=FILENAME
			sub(".*/", "", domain)
			print domain, $2, $3
		}
	}' "$DNS_DIR"/*dn42 > "$FILE_DOMAIN"

	awk '{ print $1 ".\tIN\tNS\t", $2 "." }' "$FILE_DOMAIN" | sort -u > "$FILE_DOMAIN_ENTRYS"
} &

wait

# and now build the zone
ctime=$(date +%s)
TTL=300

## build dn42.
{
	echo "\$TTL $TTL\ndn42. IN SOA ${primary}. ${contact}. ($ctime 14400 3600 1209600 300)\ndn42. IN TXT \"build and hosted by ${contact}\""
	## GLUE entrys
	# build glue entrys from INETNUM, INET6NUM
	cat "$FILE_ARPA" "$FILE_ARPA6" "$FILE_DOMAIN" | sort -u | awk '{
		if ($3 != "") {
			if ($3~ /[0-9]+\.[0-9.]+/) {
				domain=$2 "."
				sub("\.\.", ".", domain)
				if (domain~ /dn42\.$/) { print domain "\tIN\tA\t" $3 }
				}
			else {
				if ($3~ /[0-9a-fA-F]*:[0-9a-fA-F:]+/) {
					domain=$2 "."
					sub("\.\.", ".", domain)
					if (domain~ /dn42\.$/) { print domain "\tIN\tAAAA\t" $3 }
				}
			}
		}
	}' | sort -u
	cat "$FILE_DOMAIN_ENTRYS"
} > "$FILE_ZONE_dn42" &

## build 20.172.in-addr.arpa.
{
	echo "\$TTL $TTL\n20.172.in-addr.arpa. IN SOA ${primary}. ${contact}. ($ctime 14400 3600 1209600 300)"
	awk '$1~ /\.20\.172\.in-addr\.arpa\.$/ { print $0 }' "$FILE_ARPA_ENTRYS"
} > "$FILE_ZONE_rev_20" &

## build 22.172.in-addr.arpa.
{
	echo "\$TTL $TTL\n22.172.in-addr.arpa. IN SOA ${primary}. ${contact}. ($ctime 14400 3600 1209600 300)"
	awk '$1~ /\.22\.172\.in-addr\.arpa\.$/ { print $0 }' "$FILE_ARPA_ENTRYS"
} > "$FILE_ZONE_rev_22" &

## build 23.172.in-addr.arpa.
{
	echo "\$TTL $TTL\n23.172.in-addr.arpa. IN SOA ${primary}. ${contact}. ($ctime 14400 3600 1209600 300)"
	awk '$1~ /\.23\.172\.in-addr\.arpa\.$/ { print $0 }' "$FILE_ARPA_ENTRYS"
} > "$FILE_ZONE_rev_23" &

## build d.f.ip6.arpa.
{
	echo "\$TTL $TTL\nd.f.ip6.arpa. IN SOA ${primary}. ${contact}. ($ctime 14400 3600 1209600 300)"
	cat "$FILE_ARPA6_ENTRYS"
} > "$FILE_ZONE_rev_fd" &

wait
